#!/bin/sh

work_dir=$(dirname $0)

jicofo_secret=$(pass perso/Servers/Jitsi/jicofo/secret)
jicofo_password=$(pass perso/Servers/Jitsi/jicofo/password)
jvb_password=$(pass perso/Servers/Jitsi/jvb/password)

echo "JICOFO_COMPONENT_SECRET=${jicofo_secret}" >$work_dir/jicofo-secrets.env
echo "JICOFO_AUTH_PASSWORD=${jicofo_password}" >>$work_dir/jicofo-secrets.env

echo "JVB_AUTH_PASSWORD=${jvb_password}" >$work_dir/jvb-secrets.env
echo "JICOFO_AUTH_PASSWORD=${jicofo_password}" >>$work_dir/jvb-secrets.env

echo "JICOFO_COMPONENT_SECRET=${jicofo_secret}" >$work_dir/prosody-secrets.env
echo "JICOFO_AUTH_PASSWORD=${jicofo_password}" >>$work_dir/prosody-secrets.env
echo "JVB_AUTH_PASSWORD=${jvb_password}" >>$work_dir/prosody-secrets.env
